package com.pahne.sandbox.java;

import static org.assertj.core.api.Assertions.assertThat;

import com.landawn.streamex.StreamEx;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


@RunWith(JUnit4.class)
public class StreamExFeatures extends TestCase {

    Consumer<Object> print = (o) -> System.out.println(o);

    @Test
    public void compatible() throws Exception {
        Stream<String> stream = StreamEx.of("Peter", "Paul", "Mary");
        stream.forEach(print);
    }

    @Test
    public void conversion() throws Exception {
        Stream<String> stream = Stream.of("Peter", "Paul", "Mary");
        StreamEx<String> converted = StreamEx.of(stream);
        converted.forEach(print);
    }


    @Test
    public void collector_shortcut() throws Exception {
        List<String> result = StreamEx.of("Peter", "Paul", "Mary")
            .toList();
        assertThat(result).hasSize(3);
    }

    @Test
    public void append() throws Exception {
        List<String> result = StreamEx.of("Peter", "Paul")
            .append("Mary")
            .toList();
        assertThat(result).hasSize(3);
    }






}
